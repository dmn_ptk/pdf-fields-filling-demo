#Summary
1. fpdm - works partially
2. pdftk - works with Adobe Acrobat DC, paid
3. pdf-fill-form (poppler) - works with LibreOffice Writer, free of charge

# Resources
## pdf-fill-form
```
$ sudo apt-get install libpoppler-qt5-dev libcairo2-dev # poppler - the pdf tool
$ sudo apt-get install build-essential # gcc-c++
$ npm install pdf-fill-form # node poppler wrapper
```

## FPDM
http://www.fpdf.org/en/script/script93.php

# Commands
## Image magic PDF to PNG
```
convert -density 300 input.pdf -quality 95 output.png
```

## Fix PDF for FPDM
```
pdftk broken.pdf output fixed.pdf
```