<?php
// all is working
declare(strict_types=1);
require("vendor/autoload.php");

use mikehaertl\pdftk\Pdf;

$pdf = new Pdf('input/GR-fields.pdf');
// Fill in UTF-8 compliant form data!
$pdf->fillForm([
    'name'    => 'Test',
    'vorname' => 'Test',
    'strasse' => 'Test',
    'phone1'   => '1111111111',
    'phone2'   => '1111111111',
    'VersNr'   => '1',
    'sex-female'   => 'Off',
    'sex-male'   => 'On',
    'verbands-name-adress1'   => 'Test',
])
//    ->flatten()
    ->saveAs('output/example2-pdftk.pdf');

// Alternatively: Send to browser for download...
//$pdf->send('filled.pdf');

// ... or inline display
//$pdf->send();