<?php
# no radio buttons and checkboxes

declare(strict_types=1);
require('vendor/fpdm/fpdm.php');

$fields = array(
    'name'    => 'Test',
    'surname' => 'Test',
    "sex-male" => "Off",
    "sex-female"=> "", // = on
    "check1" => true,
);

$pdf = new FPDM('input/test-form-writer.pdf');


$pdf->Load($fields, true); // second parameter: false if field values are in ISO-8859-1, true if UTF-8

$pdf->Merge();
$pdf->Output('F', 'output/example4-fpdm-file-writer-created.pdf');