<?php

// will fill form inputs but they will be not visible

declare(strict_types=1);
require('vendor/fpdm/fpdm.php');

$fields = array(
    'name'    => 'Test',
    'vorname' => 'Test',
    'strasse' => 'My /Street',
    'phone1'   => '123456789',
    'phone2'   => '123456789',
//    'GetDatum'   => '2019-01-01',
    'VersNr'   => '1',
    'sex-female'   => true,
    'verbands-name-adress1'   => 'Address1',
);

$pdf = new FPDM('input/GR-fixed2.pdf');


$pdf->Load($fields, true); // second parameter: false if field values are in ISO-8859-1, true if UTF-8

$pdf->Merge();
$pdf->Output('F', 'output/example1-fpdm.pdf');