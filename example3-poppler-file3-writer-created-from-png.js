// will fill forms from custom pdf created in writer based on picture of original pdf

var pdfFillForm = require('pdf-fill-form');
var fs = require('fs');

pdfFillForm.write('input/GR-from-writer.pdf',
    {
        "name": "Test",
        // "surname": "Test",
        "sex-male": "Off",
        "sex-female": "", // = on
        // "check1": true,
    }, {
        "save": "pdf",
        // 'cores': 4,
        // 'scale': 0.2,
        // 'antialias': true
    })
    .then(function (result) {
        fs.writeFile("output/example3-poppler-file3-writer-created-from-png.pdf", result, function (err) {
            if (err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    }, function (err) {
        console.log('Error!\n');
        console.log(err);
    });