// will fill forms from custom file

var pdfFillForm = require('pdf-fill-form');
var fs = require('fs');

pdfFillForm.write('input/test-form-writer.pdf',
    {
        "name": "Test",
        "surname": "Test",
        "sex-male": "Off",
        "sex-female": "", // = on
        "check1": true,
    }, {
        "save": "pdf",
        // 'cores': 4,
        // 'scale': 0.2,
        // 'antialias': true
    })
    .then(function (result) {
        fs.writeFile("output/example3-poppler-file2-writer-created.pdf", result, function (err) {
            if (err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    }, function (err) {
        console.log('Error!\n');
        console.log(err);
    });